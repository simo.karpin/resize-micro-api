const express = require('express');
const sharp = require('sharp');
const axiosBase = require('axios');
const helmet = require('helmet');
const cors = require('cors');
const winston = require('winston');
const dotenv = require('dotenv');
dotenv.config();

const app = express();
const port = process.env.PORT || 3001;

// Logging Configuration
const logger = winston.createLogger({
    level: process.env.LOG_LEVEL || 'info',
    format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'combined.log' }),
    ],
});

// Axios Configuration
const axios = axiosBase.create({
    responseType: 'arraybuffer', // Default for image requests
    timeout: 5000, // 5 seconds timeout
});

// Axios Interceptors for logging
axios.interceptors.request.use(request => {
    logger.info(`Requesting ${request.url}...`);
    return request;
}, error => {
    logger.error(`Request error: ${error}`);
    return Promise.reject(error);
});

axios.interceptors.response.use(response => {
    logger.info(`Received response from ${response.config.url}`);
    return response;
}, error => {
    logger.error(`Response error: ${error}`);
    return Promise.reject(error);
});

// Valid Referers Configuration
let validReferers = process.env.VALID_REFERERS ? process.env.VALID_REFERERS.split(',') : [];

// Middleware Configuration
const corsOptionsDelegate = (req, callback) => {
    let corsOptions;
    if (validReferers.includes(req.header('Origin'))) {
        corsOptions = { origin: true };
    } else {
        corsOptions = { origin: false };
    }
    callback(null, corsOptions);
};

app.use(helmet());
app.disable('x-powered-by');
app.options('*', cors(corsOptionsDelegate));

// Image Resize Route
app.get('/resize', cors(corsOptionsDelegate), async (req, res) => {
    logger.info(`Received resize request: full URL=${req.protocol}://${req.get('host')}${req.originalUrl}, Query Params=${JSON.stringify(req.query)}`);
    const { orgUrl, width, height } = req.query;
    logger.info(`Received resize request: orgUrl=${orgUrl}, width=${width}, height=${height}`);

    const parsedWidth = parseInt(width);
    const parsedHeight = parseInt(height);

    if (!parsedWidth || !parsedHeight || isNaN(parsedWidth) || isNaN(parsedHeight)) {
        logger.warn(`Invalid width or height parameters: width=${parsedWidth}, height=${parsedHeight}`);
        return res.status(400).json({ error: 'Invalid width or height parameters' });
    }

    try {
        const imageResponse = await axios({ url: orgUrl });
        const image = await sharp(Buffer.from(imageResponse.data))
            .resize(parsedWidth, parsedHeight)
            .toBuffer();

        res.set('Content-Type', 'image/jpeg');
        res.set('Cache-Control', 'public, max-age=5184000');
        res.send(image);
    } catch (error) {
        logger.error(`Error processing image: ${error}`);
        res.status(500).send('Error processing image');
    }
});

// Fallback for non-existent routes
app.use((req, res) => res.status(404).send("Sorry, can't find that!"));

// Global error handler
app.use((err, req, res, next) => {
    logger.error(`Internal server error: ${err.stack}`);
    res.status(500).send('Something broke!');
});

// Starting the server
app.listen(port, () => {
    logger.info(`Server running on port ${port}`);
    if (validReferers.length > 0) {
        logger.info(`CORS is enabled for: ${validReferers.join(', ')}`);
    } else {
        logger.info(`CORS is disabled`);
    }
});
