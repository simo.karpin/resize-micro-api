# Use an official Node.js runtime as the base image
FROM node:14

# Set the working directory in the container
WORKDIR /app

# Copy the package.json file to the container
COPY package*.json ./

# Install the application's dependencies
RUN npm install

# Copy the rest of the application code to the container
COPY . .

EXPOSE 3001/tcp

# Specify the command to run the application
CMD [ "node", "index.js" ]
