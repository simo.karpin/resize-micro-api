# Resize Micro API

The Resize Micro API is a small microservice that allows you to resize images using a simple REST API. You can use it to resize images to a specific width and height, or to scale them by a specific factor. The API is built using Node.js and the Express framework.

## Getting Started

To get started, you'll need to install Node.js and the required dependencies. You can do this by running:

> __npm install__

Once you've installed the dependencies, you can start the server by running:

>node index.js

The server will start listening on port 3001 by default. You can then use the API to resize images.

This project requires the following environment variables to be set:

- `VALID_REFERERS`: A comma-separated list of valid referrers for the API. If a request comes from a referrer that is not in this list, the API will return an error response.

To set the environment variables, create a file named `.env` in the root directory of the project and add the following lines:
>VALID_REFERERS=localhost,localhost:3001

## API Endpoints

The Resize Micro API provides the following endpoints:

### `GET /resize`

This endpoint allows you to resize an image to a specific width and height. You can specify the width and height as query parameters. For example:

> http://localhost:3001/resize?width=500&height=500&orgUrl=http://example.com/image.jpg

This will resize the image at `http://example.com/image.jpg` to a width of 500 pixels and a height of 500 pixels.

## Contributing

If you'd like to contribute to the Resize Micro API, please fork the repository and submit a pull request. We welcome contributions of all kinds, including bug fixes, feature additions, and documentation improvements.

## License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT) (or Apache 2.0 License).

The MIT License (MIT) (or Apache 2.0 License) is a permissive open-source license that allows you to use, copy, modify, and distribute the software as long as the original copyright and license notice is included. This license does not provide any warranty and is used at your own risk.

For more information about the MIT License (or Apache 2.0 License), please see the [Open Source Initiative](https://opensource.org/licenses/MIT) (or [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)).
